#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include "VG/openvg.h"
#include "VG/vgu.h"
#include "fontinfo.h"
#include "shapes.h"
#define BAUDRATE B115200 

void setupPI();

int width = 300, height = 300;
char s[3];

struct termios serial;
char *dev_id = "/dev/serial0";
uint8_t str[256] = {}; //data to be transmitted

char rxBuffer[256] = {};
unsigned long numberTotal = 0;
int fd;

typedef struct packet{
    uint8_t voltage;
    uint8_t channel;
    uint8_t hLevel;
} packet_t;
int dummy;

packet_t rxPackets[256] = {};
double x, y;

//void createPointBlue(int x, int y, 

uint8_t chHistory[2][256] = {};

enum {FREE_RUNNING, TRIGGER, YES_TRIGGER, NO_TRIGGER, TRIGGER_SLOPE_POSITIVE, TRIGGER_SLOPE_NEGATIVE};
void graphVoltages(int xscale, float yscale, int channelNumber, int hLevel){
    //first channel
    int xVal = 255;
    int yVal = 0;
    StrokeWidth(2);
    
    if (channelNumber == 0)
        Stroke(255, 255, 0, 1); //pink ch1
    else
        Stroke(255, 0, 0, 1);

    int i = 0;
    int drawX = 1920;//start x for time of both channels 
    int drawY = 500 - (hLevel * 2); //start y to draw at middle of screen
    //draw first channel
    for (i = 255; i >= 0 ; ){
       
       int yValPrev = chHistory[channelNumber][( ((i == 0) || (i == 255) ) ? i  : i - 1)] * yscale;
       yVal = (float)((float)chHistory[channelNumber][i] * (float)(yscale)); //voltage value apply yscaling
       //plot the first point
       //apply scaling -- repeat this same voltage for xscale times
       int j = 0;
       //for (j = 0; j < xscale; j++){
        
            Line(drawX, drawY + yValPrev, drawX + xscale, drawY + yVal);
            //Line(drawX - j, drawY + yVal, drawX+xscale , drawY + yValPrev);
            
       //}
       drawX = drawX - (1 * xscale);
       i = i - 1; //go to the next voltage value in the history 
    }
}
void shiftGraphLeft(uint8_t *ch1Hist, uint8_t *ch2Hist);

void addVoltageToHistory(int channelN, int voltage);

void drawOscilloscopeBackground(){
    int i = 0; 
    StrokeWidth(1);
    Stroke(0, 0, 255, 1);
    for (i = 0; i < 1920; i = i + 150){
        
        Line(i, 0 , i, 1200);
    }
    for (i = 0; i < 1200; i = i + 150){
        
        Line(0, i, 1900, i);
    }
    Fill(255, 255, 255, 1);
    }
void drawOscilloScreen(int nChannels, int xscale, float yscale, int hLevelCurrent){ //graph channels. calls graph voltages
        if (xscale == 0){ //set default value
            xscale = 10;
        }
        if (yscale == 0.0){
            yscale = 1.0;
        }

      Start(width, height);					// Start the picture
      Background(0, 0, 0);					// green background
      drawOscilloscopeBackground();
      char chDisp[1024] = {};
      sprintf(chDisp, "Kelvins Oscilloscope, xScale: %i microseconds/div, yScale: %f volts/div, potVal: %i, nChannels: %i", xscale, yscale,
      hLevelCurrent, nChannels);
      Text(50, 50, chDisp ,SerifTypeface, 10);

        if (nChannels > 1){

            graphVoltages(xscale, yscale, 0, 0);
            graphVoltages(xscale, yscale, 1, hLevelCurrent); //graph 2 channels
        }else {
            graphVoltages(xscale, yscale, 0, 0);
        }
      End();						   			// End the picture

}

//rotation algorithm by Joe Bentley programming pearls columnn 2
//stackoverflow
void reverse(uint8_t a[], int sz){
    int i, j;
    for (i = 0, j = sz; i < j; i++, j--){
        
        uint8_t tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
}
void rotate(uint8_t array[], int size, int amt){
    if (amt < 0)
        amt = size + amt;
    reverse(array, size-amt-1);
    reverse(array+size-amt, amt-1);
    reverse(array, size-1);
}


void oscilloscopeFunction(int yesNoTrigger, int trigLevel, int trigSlope,int trigCh,  int nChannels, int yscale, int xscale){
    

    x = 10.0;
    y = 10.0;
    int chsHistory[2] = {255, 255};

    /*for(i = 0; i < 256; i++){
        ch1History[i] = (i % 256);
    }*/
    int chHistoryShifted[256] = {};
        //to transmit: int wcount = write(fd, str, 256);
    //to read: int read_bytes = read(fd, &rxxbuffer[rcount], 1);

    packet_t newPacket;

    setupPI();
    char newChar = 0;
    int rcount = 1;
    int hLevelCurrent = 0;
	init(&width, &height);				// Graphics initialization
    initWindowSize(1000, 1000, 1000, 1000);

    //free running mode
    int circularCounter = 0;
    int triggerHasReached = 1;
    int savePosTrigger = 0;
    
    for (;;){

        if (yesNoTrigger == NO_TRIGGER){ //graph freerunning historry -- when all is filled
            if ( (chsHistory[0] == 255) && (chsHistory[1] == 255)){ //when all chs history have been collected, graph.
                drawOscilloScreen(nChannels, xscale, yscale, hLevelCurrent);
            }
        }else {
            //test trigger for draw
            //trigCh-1 because user will input "1" or "2" 


             if (trigLevel == chHistory[trigCh - 1][chsHistory[trigCh-1]]){
                  int indexNext = (chsHistory[trigCh-1] == 0) ? 255 : chsHistory[trigCh-1]-1;
                  int next = chHistory[trigCh-1][indexNext]; //assert slope conditions for trigg


                  if ( (trigSlope == TRIGGER_SLOPE_POSITIVE) && (next < trigLevel) ){

                    rotate(&chHistory[trigCh-1][0], 256, (255 - chsHistory[trigCh-1])); //rotate to get stable graph
                    drawOscilloScreen(nChannels, xscale, yscale, hLevelCurrent);
                    chsHistory[trigCh-1] = 255; //slew back to the left
                        //good to draw
                 }else if ( (trigSlope == TRIGGER_SLOPE_NEGATIVE) && (next < trigLevel)){

                    rotate(&chHistory[trigCh-1][0], 256, (255 - chsHistory[trigCh-1])); //rotate to get stable graph
                    drawOscilloScreen(nChannels, xscale, yscale, hLevelCurrent);
                    chsHistory[trigCh-1] = 255; //slew back to the left
                 }
                   
             }
                    
             
        }


        int read_bytes = read(fd, &rxPackets[0], sizeof(packet_t));
        if (read_bytes < 0){
            perror("Read");
            exit(0);
        }else if (read_bytes == sizeof(packet_t)) {
        // }else if (read_bytes > 0){   
        
            if (rxPackets[0].channel == 0)
                printf( "\nvoltage: %i, channel: %i hLevel: %i", rxPackets[0].voltage, rxPackets[0].channel,rxPackets[0].hLevel);
            else if (rxPackets[0].channel == 1)
                 printf( "\nvoltage: %i, channel: %i hLevel: %i", rxPackets[0].voltage, rxPackets[0].channel,rxPackets[0].hLevel);

           if (rxPackets[0].voltage >= 250){
               rxPackets[0].voltage = 250;
           }else if (rxPackets[0].voltage <= 5){
               rxPackets[0].voltage = 0;
           }
           
           if ((rxPackets[0].channel == 0) || (rxPackets[0].channel == 1)){ //make sure no segfault

               chHistory[ rxPackets[0].channel ][ chsHistory[rxPackets[0].channel] ] = rxPackets[0].voltage; //keep track of history
               
               chsHistory[rxPackets[0].channel]--; //keep track of history index. ::DECREMENT HERE
               if (rxPackets[0].channel == 1){ //keep track of hlevel
                   hLevelCurrent = rxPackets[0].hLevel;
               }
               
               //slew to the other end and replace history 
               if (chsHistory[rxPackets[0].channel] <= 0){
                    
                    chsHistory[rxPackets[0].channel] = 255;
                    
               }
           }
           // printf( " %c ", rxBuffer[1]);
           // scanf("%i", &dummy);
 //           delay(100);
        }else { //did not read anything
       
        }
        
    }
    
    /*for(i = 0; i < 5; i++){
        printf(" %i ", rxBuffer[i]);
    }*/

/*
	fgets(s, 2, stdin);				   		// look at the pic, end with [RETURN]
	finish();					            // Graphics cleanup
    */
    finish();
    close(fd);
	exit(0);

}

int main() {
    int nchannels = 0;
    int mode = FREE_RUNNING;
    int triggerLevel = 0;
    int triggerSlope = 0;
    int triggerChannel = 0;
    float yScale = 0;
    int xScale = 0;
    char set[255] = {};
    char mod[255] = {};

    for(;;){

       fseek(stdin, 0, SEEK_END);
   //    printf("%i, %i, %i, %i, %i, %f, %i,", nchannels, mode, triggerLevel, triggerSlope, triggerChannel, yScale,
   //    xScale); 
        scanf("%s",set);
        if (strcmp(set, "set") == 0){

//            printf("HERE");
            scanf("%s", set);
            if (strcmp(set, "mode") == 0){
                scanf("%s", set);
                if (strcmp(set, "free_run") == 0){
                   mode = FREE_RUNNING;
                }else if (strcmp(set, "trigger") == 0){
                    mode = TRIGGER;
                }else {
                    printf("ERROR TYPE AGAIN");
                    
                    fseek(stdin, 0, SEEK_END);
                }
            }else{

                if (strcmp(set, "nchannels") == 0){
                    
                    scanf("%d", &nchannels);
                }else if (strcmp(set, "trigger_level") == 0){
                    
                    scanf("%d", &triggerLevel);
                }else if (strcmp(set, "trigger_slope") == 0){
                    
                    scanf("%d", &triggerSlope);
                }else if (strcmp(set, "trigger_channel") == 0){
                    
                    scanf("%d", &triggerChannel);
                }else if (strcmp(set, "yscale") == 0){

                    scanf("%f", &yScale);
                }else if (strcmp(set, "xscale") == 0){

                    scanf("%d", &xScale);
                }else {
                    printf("ERROR TYPE AGAIN");

                    fseek(stdin, 0, SEEK_END);
                }

            }
 //           printf("Good");
        }else if (strcmp(set, "start") == 0){
        
            //start here get params...
            if (mode == FREE_RUNNING){
                //send params and start
                //with NO_TRIGGER trigger settings params are ignored
                oscilloscopeFunction(NO_TRIGGER, 0, 0, 0, nchannels, yScale, xScale);

            }else if (mode == TRIGGER){
                //send more params and start
                int trigSlopeFlag = (triggerSlope ? TRIGGER_SLOPE_POSITIVE : TRIGGER_SLOPE_NEGATIVE);
                oscilloscopeFunction(YES_TRIGGER, triggerLevel, trigSlopeFlag, triggerChannel, nchannels, yScale, xScale); 
            }

        }else {

            printf("ERROR TYPE AGAIN\n");

            fseek(stdin, 0, SEEK_END);
        }

        fseek(stdin, 0, SEEK_END);
    }

}

void setupPI(){
    fd = open(dev_id, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1){
        perror(dev_id);
        exit(-1);
    }
    if (tcgetattr(fd, &serial) < 0){
        perror("Getting configuration");
        exit(-1);
    }
    serial.c_iflag = IGNPAR;
    serial.c_oflag = 0;
    serial.c_lflag = 0;
    serial.c_cflag = BAUDRATE | CS8 |CREAD | PARENB  | PARODD;
    serial.c_cc[VMIN] = 0;
    serial.c_cc[VTIME] = 0;

    tcsetattr(fd, TCSANOW, &serial);
    wiringPiSetup();
}
    //    Stroke(44, 77, 232, 1);
     //   Line(0,0, 30, 30);

//        Fill(44, 77, 232, 1);					// Big blue marble
        //Circle(width / 2, 0, width);			// The "world"
        //Fill(255, 255, 255, 1);					// White text
        //Text(2,2 , "Kelvins Oscilloscope", SerifTypeface, 1);	// Greetings 
 
