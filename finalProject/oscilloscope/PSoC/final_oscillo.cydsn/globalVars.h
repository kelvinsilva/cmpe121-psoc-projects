/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef GLOBAL_VARS_H_
#define GLOBAL_VARS_H_
    
typedef struct packet{
    uint8_t voltage;
    uint8_t channel;
    uint8_t hLevel;
} packet_t; 
extern uint8_t voltage1;
extern uint8_t voltage2;
extern uint8_t dataReady1;
extern uint8_t dataReady2;

#endif
/* [] END OF FILE */
