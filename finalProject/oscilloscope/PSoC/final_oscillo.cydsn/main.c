/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "globalVars.h"



int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    
    UART_1_Start();
    UART_1_Enable();
    
    
    ADC_SAR_1_Start();
    ADC_SAR_1_StartConvert();
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    ADC_DelSig_1_Start();
    ADC_DelSig_1_Enable();
    ADC_DelSig_1_StartConvert();
    
    
    ADC_SAR_2_Start();
    ADC_SAR_2_StartConvert();
    
    
    uint8_t x = 0;
    
    for(;;)
    {

        uint16_t hLevel =  ADC_DelSig_1_Read32() / 257;

        if (hLevel < 0){
            hLevel = 2;
        }else if (hLevel > 250){
            hLevel = 255;
        }
        
        if (ADC_SAR_1_IsEndConversion(ADC_SAR_1_WAIT_FOR_RESULT)){ //will block
            packet_t sendPack1;
            voltage1 = ADC_SAR_1_GetResult8();
            sendPack1.channel = 0;
            sendPack1.hLevel = hLevel;
            sendPack1.voltage = voltage1;
            UART_1_PutArray(&sendPack1, sizeof(sendPack1)); //will block
        }
         
        if (ADC_SAR_2_IsEndConversion(ADC_SAR_1_WAIT_FOR_RESULT)){ //will block
            packet_t sendPack2;
            voltage2 = ADC_SAR_2_GetResult8();
            sendPack2.channel = 1;
            sendPack2.hLevel = hLevel;
            sendPack2.voltage = voltage2;
            UART_1_PutArray(&sendPack2, sizeof(sendPack2)); //will block
        }

    }
    ADC_SAR_1_EndConvert();
}

/* [] END OF FILE */
