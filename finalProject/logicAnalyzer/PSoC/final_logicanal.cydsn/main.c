/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"


typedef struct packet {
        
    uint8_t ch0Info : 1; 
    uint8_t ch1Info : 1;
    uint8_t ch2Info : 1;
    uint8_t ch3Info : 1;
    uint8_t ch4Info : 1;
    uint8_t ch5Info : 1;
    uint8_t ch6Info : 1;
    uint8_t ch7Info : 1;
    uint8_t potInfo;    
}packet_t;

typedef union{
    packet_t pack;
    uint8_t allChs;
}packet_union;

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    UART_1_Start();
    UART_1_Enable();
    
    Counter_1_Start();
    Counter_1_Enable();
    
    
    ADC_DelSig_1_Start();
    ADC_DelSig_1_Enable();
    ADC_DelSig_1_StartConvert();
    packet_union myPacket;
   
    for(;;)
    {
        /* Place your application code here. */
        
        uint16_t hLevel =  ADC_DelSig_1_Read32() / 257;

        if (hLevel < 0){
            hLevel = 2;
        }else if (hLevel > 250){
            hLevel = 255;
        }
        
        
        uint8_t statusChs = Status_Reg_1_Read();
        myPacket.allChs = Counter_1_ReadCounter();//statusChs;
        myPacket.pack.potInfo = hLevel;
        
        UART_1_PutArray(&myPacket, sizeof(packet_t));
        CyDelay(1);
    }
}

/* [] END OF FILE */
