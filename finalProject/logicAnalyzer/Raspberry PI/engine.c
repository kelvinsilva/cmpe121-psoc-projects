#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include "VG/openvg.h"
#include "VG/vgu.h"
#include "fontinfo.h"
#include "shapes.h"
#define BAUDRATE B115200 


/*DO NOT MODIFY COMMENT BELOW DO NOT TOUCH LINE BELOW*/
/*!@!*/
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/**/

#define SETTING_VIEWING_SIZE 16 
typedef struct packet{
         
        uint8_t ch0Info : 1;
        uint8_t ch1Info : 1;
        uint8_t ch2Info : 1;
        uint8_t ch3Info : 1;
        uint8_t ch4Info : 1;
        uint8_t ch5Info : 1;
        uint8_t ch6Info : 1;
        uint8_t ch7Info : 1;    
        uint8_t potInfo;
} packet_t;

typedef union{

    packet_t pack;
    uint8_t allChs;

} packet_union;

enum {FREE_RUNNING, TRIGGER, YES_TRIGGER, NO_TRIGGER, TRIGGER_SLOPE_POSITIVE, TRIGGER_SLOPE_NEGATIVE};

void setupPI();

void drawOscilloscopeBackground();

void graphVoltages(uint8_t histWindow[], int size, int histStartIndex, int histEndIndex, int triggerIndex, int xScale, int channelNumber, int xLevel, int memDepth, int hLevel);
void drawOscilloScreen(int nChannels, int xScale, int xLevel, int memDepth, int triggerIndex);
void shiftGraphLeft(uint8_t *ch1Hist, uint8_t *ch2Hist);
void addVoltageToHistory(int channelN, int voltage);

int width = 300, height = 300;
char s[3];

struct termios serial;
char *dev_id = "/dev/serial0";
uint8_t str[256] = {}; //data to be transmitted

char rxBuffer[256] = {};
unsigned long numberTotal = 0;
int fd;

int dummy;

packet_union rxPackets[KELVIN_MEM_DEPTH] = {};
double x, y;

//void createPointBlue(int x, int y, 

uint8_t chHistory[KELVIN_MEM_DEPTH] = {};
int chsHistory = 0;
//trigger monitors
uint8_t ch0 = 0, ch1 = 0, ch2 = 0, ch3 = 0, ch4 = 0, ch5 = 0, ch6 = 0, ch7 = 0;

//rotation algorithm by Joe Bentley programming pearls columnn 2
//stackoverflow
void reverse(uint8_t a[], int sz);
void rotate(uint8_t array[], int size, int amt);
//end of rotation algorithm

void printDebug();

void logicAnalFunction(int triggerDir, int memDepth, int sampleFreq, int nChannels,  int xScale){
    
    x = 10.0;
    y = 10.0;

	init(&width, &height);				// Graphics initialization
    initWindowSize(1000, 1000, 1000, 1000);

    for (;;){
        int read_bytes = read(fd, &rxPackets[0], sizeof(packet_t));
        if (read_bytes < 0){
            perror("Read");
            exit(0);
        }else if (read_bytes == sizeof(packet_t)) {
        // }else if (read_bytes > 0){   
        
           printDebug(); //print debug
           chHistory[ chsHistory ] = (uint8_t)rxPackets[0].allChs; //keep track of history
           
           chsHistory--;
           //slew to the other end and replace history 
            //keep trigger monitors updated
           ch0 = rxPackets[0].pack.ch0Info;
           ch1 = rxPackets[0].pack.ch1Info;
           ch2 = rxPackets[0].pack.ch2Info;
           ch3 = rxPackets[0].pack.ch3Info;
           ch4 = rxPackets[0].pack.ch4Info;
           ch5 = rxPackets[0].pack.ch5Info;
           ch6 = rxPackets[0].pack.ch6Info;
           ch7 = rxPackets[0].pack.ch7Info;


           if (chsHistory <= 0){
                
                chsHistory = memDepth - 1;
           }
       }
       
       /*DO NOT MODIFY THE COMMENT BELOW, CODE INJECT IN HERE, DO NOT TOUCH THE LINE BELOW*/
/*!!@*/
/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
        { //-----------------------__TRIGGER POINT__---------------------------------------

               //if trigger, keep grabbing more history to see after effects
               //specifically, grab KELVIN_MEM_DEPTH/2 more samples after the trigger point
               int numSamplesCollected = 0;
               int triggerIndex = chsHistory; //save index of trigger point
               int prevPotVal = 0;
               while(1){ //forever need to loop, collect rest of history, then keep looping to obtain pot slew value 

                    int read_bytes = read(fd, &rxPackets[0], sizeof(packet_t));
                    if (read_bytes < 0){
                        perror("Read");
                        exit(0);
                    }else if (read_bytes == sizeof(packet_t)) {
                       
                       if (numSamplesCollected < (memDepth/2) ){ //keep collecting samples and adding to history
                           chsHistory--; //before collect, move the history counter here
                           chHistory[ chsHistory ] = (uint8_t)rxPackets[0].allChs; 
                           numSamplesCollected++;
                           //slew to the other end and replace history 
                           if (chsHistory <= 0){
                                    
                                chsHistory = memDepth - 1;
                           }
                       }else { //history collection has ended. Now need to keep collecting packets for pot slew val
                            //collect pot slew val and draw to screen if pot val change, draw new oscilloscrn
                            int cpt = rxPackets[0].pack.potInfo;
                            if ( (prevPotVal != cpt) ){
                                prevPotVal = cpt;
                                drawOscilloScreen(nChannels, xScale, cpt, memDepth, triggerIndex);            
                            }
                       }
                    }
               }
                //at end of sample collect. Need to display waveforms and keep polling psoc for pot reading for slew
            }
    }   
    finish();
    close(fd);
	exit(0);

}

int main() {
    setupPI();
    logicAnalFunction(KELVIN_TRIG_DIR, KELVIN_MEM_DEPTH, KELVIN_SAMPLE_FREQ, KELVIN_NCH, KELVIN_XSCAL);
    return 0;
}

void reverse(uint8_t a[], int sz){
    int i, j;
    for (i = 0, j = sz; i < j; i++, j--){
        
        uint8_t tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
}

void rotate(uint8_t array[], int size, int amt){
    if (amt < 0)
        amt = size + amt;
    reverse(array, size-amt-1);
    reverse(array+size-amt, amt-1);
    reverse(array, size-1);
}

void setupPI(){
    fd = open(dev_id, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1){
        perror(dev_id);
        exit(-1);
    }
    if (tcgetattr(fd, &serial) < 0){
        perror("Getting configuration");
        exit(-1);
    }
    serial.c_iflag = IGNPAR;
    serial.c_oflag = 0;
    serial.c_lflag = 0;
    serial.c_cflag = BAUDRATE | CS8 |CREAD | PARENB  | PARODD;
    serial.c_cc[VMIN] = 0;
    serial.c_cc[VTIME] = 0;

    tcsetattr(fd, TCSANOW, &serial);
    wiringPiSetup();
}
    //    Stroke(44, 77, 232, 1);
     //   Line(0,0, 30, 30);

//        Fill(44, 77, 232, 1);					// Big blue marble
        //Circle(width / 2, 0, width);			// The "world"
        //Fill(255, 255, 255, 1);					// White text
        //Text(2,2 , "Kelvins Oscilloscope", SerifTypeface, 1);	// Greetings 

 void graphVoltages(uint8_t histWindow[], int size, int histStartIndex, int histEndIndex, int triggerIndex, int xScale, int channelNumber, int xLevel, int memDepth, int hLevel){

    
    //first channel
    int drawX = 110;//start x for time of channel 
    int drawY = 900 - (hLevel*2); //start y to draw at middle of screen
    int yScaleCustom = 30; //set custom y scaling
    int xValTitle = 5;
    int yValTitle = drawY + yScaleCustom - 8;

    char chDisp[32] = {};
    sprintf(chDisp, "Channel %i | 1 ", channelNumber); //setup title 
    Fill(255,255,255,1);
    Text(xValTitle, yValTitle, chDisp, SerifTypeface, 10); //display ch title
    sprintf(chDisp, "                  | 0 ");
    Text(xValTitle, yValTitle - (yScaleCustom -8), chDisp, SerifTypeface, 10);

    char timeDisp[32] = {};

    StrokeWidth(3);
    Stroke(0, 255, 255, 1); //cyan ch1
    switch(channelNumber){
       case 1:
            Stroke(0, 255, 255, 1); //cyan ch 1
            break;
       case 2:
            Stroke(0, 204, 102, 1); //green ch2
            break;
       case 3:
            Stroke(255, 51, 204, 1); //pink ch3
            break;
       case 4:
            Stroke(255, 102, 0, 1); //light brown ch4
            break;
       case 5:
            Stroke(0, 0, 255, 1); //dark blue ch5
            break;
       case 6:
            Stroke(255, 255, 0, 1); //yellow ch6
            break;
       case 7:
            Stroke(153, 153, 102, 1); //gray brown ch7
            break;
       case 8:
            Stroke(255, 51, 0, 1); //dirty red ch8
            break;
       default:
            Stroke(255, 255, 204, 1); //else white
            break;
    }

    int i = 0;
    //draw channel
    for (i = size-1; i >= 0 ; i-- ){
       
       uint8_t logicNext = histWindow[ (i == 0) ? 0 : (i - 1) ] & (uint8_t)( (uint8_t)(1) << (uint8_t)(channelNumber - 1)); //get prev hist
       uint8_t logic = histWindow[i]  & (uint8_t)( (uint8_t)(1) << (uint8_t)(channelNumber - 1)); //voltage value apply yscaling
       //setup timing bars and timing display
       StrokeWidth(1);
       Stroke(0, 0, 255, 1);
       Line(drawX, 0, drawX, 1200);

       sprintf(timeDisp, "%i mSec", memDepth-histStartIndex + i + 1);
       Fill(0,255,0,1);
       Text(drawX, 1150, timeDisp, SerifTypeface, 10);


        StrokeWidth(3);
        Stroke(0, 255, 255, 1); //cyan ch1


       if (logic  > 0){ //draw a one
            Line(drawX, drawY + yScaleCustom, drawX + xScale, drawY + yScaleCustom);
            if (logicNext == 0){ //draw negedge
                Line(drawX + xScale, drawY+ yScaleCustom, drawX + xScale, drawY);
            }
       }else if (logic == 0) { //draw a zero
            Line(drawX, drawY, drawX + xScale, drawY);
            if (logicNext > 0){ //draw a posedge
                Line(drawX + xScale, drawY, drawX + xScale, drawY+ yScaleCustom);
            }
       }
       
       drawX = drawX + xScale;
    }

    if ( (histStartIndex >= triggerIndex) && (histEndIndex <= triggerIndex)){ //our vieiwing window is within trigger
        //so need to draw
        //DRAW TRIGGER LINE VERTICAL
        int drawXTrigger = (triggerIndex - histEndIndex) * xScale;
        drawXTrigger += 110; //offset from the left side (to not covver the ch display  
        StrokeWidth(1);
        Stroke(255, 0, 0, 1);
        Line(drawXTrigger, 0, drawXTrigger, 1200);
    }
}

void drawOscilloscopeBackground(){
    int i = 0; 
    StrokeWidth(1);
    Stroke(0, 0, 255, 1);
    for (i = 0; i < 1920; i = i + 150){
        
        Line(i, 0 , i, 1200);
    }
    for (i = 0; i < 1200; i = i + 150){
        
        Line(0, i, 1900, i);
    }
    Fill(255, 255, 255, 1);
    Text(50, 50, "Kelvins Logic Analyzer", SerifTypeface, 10);
}

void drawOscilloScreen(int nChannels, int xScale, int xLevel, int memDepth, int triggerIndex){ //graph channels. calls graph voltages

    //change
      Start(width, height);					// Start the picture
      Background(0, 0, 0);					// green background
      //drawOscilloscopeBackground();
      int i = 0;
      uint8_t viewingHist[SETTING_VIEWING_SIZE] = {0};
      int step = memDepth / 256; //do not change this line

      int hStartIndex = memDepth - (xLevel * step) - 1;
      int hEndIndex = hStartIndex - SETTING_VIEWING_SIZE;
      
      int j = 0;
      i = hStartIndex;
      for (j = 0; (j < SETTING_VIEWING_SIZE); j++){
          viewingHist[j] = chHistory[i];
          i--;
      }

      //memcpy(viewingHist, &chHistory[memDepth-1-64 - hStartIndex], 64);

      for (i = 0; i < nChannels; i++){
      //graph each channel. each channel is 10 + (i*40) pixels below the other
        //graphVoltages(viewingHist, 64, hStartIndex, hEndIndex, triggerIndex, xScale, 1, xLevel, memDepth, 10 + (1 * 40) ); 

        graphVoltages(viewingHist, SETTING_VIEWING_SIZE, hStartIndex, hEndIndex, triggerIndex, xScale, i+1, xLevel,
        memDepth, 10 + (i * 40) ); 


      }

      End();						   			// End the picture
}

void printDebug(){
           printf( "\nch0: %i, ch1: %i ch2: %i ch3: %i, ch4: %i, ch5: %i, ch6: %i, ch7: %i , potVal: %i", 
            rxPackets[0].pack.ch0Info,
            rxPackets[0].pack.ch1Info,
            rxPackets[0].pack.ch2Info,
            rxPackets[0].pack.ch3Info,
            rxPackets[0].pack.ch4Info,
            rxPackets[0].pack.ch5Info,
            rxPackets[0].pack.ch6Info,
            rxPackets[0].pack.ch7Info,
            rxPackets[0].pack.potInfo);

 
}
