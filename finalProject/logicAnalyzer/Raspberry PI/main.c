#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>

typedef struct packet {
    
    uint8_t ch0Info : 1;
    uint8_t ch1Info : 1;
    uint8_t ch2Info : 1;
    uint8_t ch3Info : 1;
    uint8_t ch4Info : 1;
    uint8_t ch5Info : 1;
    uint8_t ch6Info : 1;
    uint8_t ch7Info : 1;    
    uint8_t potInfo;

} packet_t;
enum {FREE_RUNNING, TRIGGER, YES_TRIGGER, NO_TRIGGER, TRIGGER_SLOPE_POSITIVE, TRIGGER_SLOPE_NEGATIVE};

void injectCompileCheckErrorAndRun(char string[], int trigDir, int memDepth, int sampleFreq, int nChs, int xScal){
    //first make copy of engine file.
    system("cp engine.c engine_to_compile.c");
    //now need to inject code in engine_to_compile.c
    //setup strings for defines
    char str1[255] = {};
    char str2[255] = {};
    char str3[255] = {};
    char str4[255] = {};
    char str5[255] = {};
    char str6[255] = {};

    //format c code
    
    sprintf(str6, "if(%s)", string);

    sprintf(str1, "#define KELVIN_TRIG_DIR %i \n", trigDir);
    sprintf(str2, "#define KELVIN_MEM_DEPTH %i \n", memDepth);
    sprintf(str3, "#define KELVIN_SAMPLE_FREQ %i \n", sampleFreq);
    sprintf(str4, "#define KELVIN_NCH %i \n", nChs);
    sprintf(str5, "#define KELVIN_XSCAL %i \n", xScal);

    
    FILE * filePtr = fopen("engine_to_compile.c", "r+");
    int dummy = 0;
    char line[1024] = {};
    while (feof(filePtr) == 0){
        
        fgets(line, 1024, filePtr);
        printf("%s", line);
        //make sure to compare with newline
        if (strcmp(line, "/*!!@*/\n") == 0){

            fwrite(str6, 1, strlen(str6), filePtr); //inject if statement
        }else if (strcmp(line, "/*!@!*/\n") == 0){

            fwrite(str1, 1, strlen(str1), filePtr); //inject trig dir define
            fwrite(str2, 1, strlen(str2), filePtr);
            fwrite(str3, 1, strlen(str3), filePtr);
            fwrite(str4, 1, strlen(str4), filePtr);
            fwrite(str5, 1, strlen(str5), filePtr);
            
        }
        memset(line, 0, 1024);
    }

    fclose(filePtr);

    if (system("gcc -g -Wall -I/opt/vc/include -I/opt/vc/include/interface/vmcs_host/linux"
               "-I/opt/vc/include/interface/vcos/pthreads engine_to_compile.c -o logicEngine.kelvin -lshapes -lwiringPi" ) == 0) { //NO ERROR CLEAN

        system("./logicEngine.kelvin"); //NO ERROR so run program. Exit NOW
        exit(0);
    }else { //ERROR return -1

        printf ("!!!!!!!!!!!!!!!!!!!!!\n\n\nUSER ERROR TRY AGAIN\n\n\n!!!!!!!!!!!!!!!!!!!!!!!");
        return -1;
    }
    
}

int main(){

    int nchannels = 0;
    int mode = FREE_RUNNING;
    int triggerDir = 0;
    int memDepth = 0;
    int sampleFreq = 0;
    int xScale = 0;
    char set[255] = {};
    char injectCode[255] = {};

    for(;;){

       fseek(stdin, 0, SEEK_END);
       printf("%i, %i, %i, %i, %i, %i" , nchannels, mode, triggerDir, memDepth, sampleFreq,
       xScale); 
        scanf("%s",set);
        if (strcmp(set, "set") == 0){

            printf("HERE");
            scanf("%s", set);
            if (strcmp(set, "mode") == 0){
                scanf("%s", set);
                if (strcmp(set, "free_run") == 0){
                   mode = FREE_RUNNING;
                }else if (strcmp(set, "trigger") == 0){
                    mode = TRIGGER;
                }else {
                    printf("ERROR TYPE AGAIN");
                    
                    fseek(stdin, 0, SEEK_END);
                }
            }else{

                if (strcmp(set, "nchannels") == 0){
                    
                    scanf("%d", &nchannels);
                }else if (strcmp(set, "trigger_cond") == 0){
                   //CAPTURE BOOLEN EXPRESION TO INJECT 
                    fgets(injectCode, 255, stdin);
//                    scanf("%s", injectCode);
                }else if (strcmp(set, "trigger_dir") == 0){
                   //set trigger dir 
                    scanf("%d", &triggerDir);
                }else if (strcmp(set, "mem_depth") == 0){
                   //set memory depth (amout) 
                    scanf("%d", &memDepth);
                }else if (strcmp(set, "sample_freq") == 0){
                    
                    scanf("%d", &sampleFreq);
                }else if (strcmp(set, "xscale") == 0){

                    scanf("%d", &xScale);
                }else {
                    printf("ERROR TYPE AGAIN");

                    fseek(stdin, 0, SEEK_END);
                }

            }
            printf("Good");
        }else if (strcmp(set, "run") == 0){
        
            
               injectCompileCheckErrorAndRun(injectCode, triggerDir, memDepth, sampleFreq, nchannels, xScale);
        }else {

            printf("ERROR TYPE AGAIN\n");

            fseek(stdin, 0, SEEK_END);
        }

        fseek(stdin, 0, SEEK_END);
    }

    return 1;
}
