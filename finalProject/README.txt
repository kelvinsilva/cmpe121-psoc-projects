This is a project by Kelvin Silva as part of the final project of CMPE 121 class at UC Santa Cruz.

We were tasked to build an Oscilloscope by using a Raspberry PI and a PSoC Microcontroller.

The microcontroller served as our "voltmeter" and captured voltage from a source and continuously measured the voltage and sent it as data to the Raspberry PI through UART.

The Raspberry PI graphed the voltages onto a computer screen to show a waveform representing voltage levels.

Another side of this project was to create a Logic Analyzer. Basically measure voltages of a system (can be high or low voltages), through multiple channels (8 channels, 8 bits), and record all voltage changes.

After a certain trigger condition is met (ex, ch1 and ch2 are high), data is displayed on the computer screen by the Raspberry PI as a histo-graph of logic signals (high low, etc).

For more design and more detailed information, read the PDF attached to the repository. 

--------------------------------------------------------------------------------------------------------
NEED TO INSTALL WIRINGPI As a dependency

Type make to compile

Type make run to run programs.

For logic analyzer, remember there is code injection, so program will copy engine.c to engine_to_compile.c and compile it.
You can delete these temp files if wanted.
But do not delete engine.c

Oscilloscope: test with sine wave waveform generator 1Vpp +1v dc offset at 1.790Khz
Logic Analyzer: can test with whatever logic sequence.
