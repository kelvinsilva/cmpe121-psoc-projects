/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        /* Place your application code here. */
        
        Pin_Led_Write(0b00000011);
        
        int i = 0; 
        for (; i < 1000000; i++){}
        
        Pin_Led_Write(0b00000111);
        for (i = 0; i < 1000000; i++){}
    }
}

/* [] END OF FILE */
