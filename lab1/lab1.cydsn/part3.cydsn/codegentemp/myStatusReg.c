/*******************************************************************************
* File Name: myStatusReg.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware to read the value of a Status 
*  Register.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "myStatusReg.h"

#if !defined(myStatusReg_sts_sts_reg__REMOVED) /* Check for removal by optimization */


/*******************************************************************************
* Function Name: myStatusReg_Read
********************************************************************************
*
* Summary:
*  Reads the current value assigned to the Status Register.
*
* Parameters:
*  None.
*
* Return:
*  The current value in the Status Register.
*
*******************************************************************************/
uint8 myStatusReg_Read(void) 
{ 
    return myStatusReg_Status;
}


/*******************************************************************************
* Function Name: myStatusReg_InterruptEnable
********************************************************************************
*
* Summary:
*  Enables the Status Register interrupt.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void myStatusReg_InterruptEnable(void) 
{
    uint8 interruptState;
    interruptState = CyEnterCriticalSection();
    myStatusReg_Status_Aux_Ctrl |= myStatusReg_STATUS_INTR_ENBL;
    CyExitCriticalSection(interruptState);
}


/*******************************************************************************
* Function Name: myStatusReg_InterruptDisable
********************************************************************************
*
* Summary:
*  Disables the Status Register interrupt.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void myStatusReg_InterruptDisable(void) 
{
    uint8 interruptState;
    interruptState = CyEnterCriticalSection();
    myStatusReg_Status_Aux_Ctrl &= (uint8)(~myStatusReg_STATUS_INTR_ENBL);
    CyExitCriticalSection(interruptState);
}


/*******************************************************************************
* Function Name: myStatusReg_WriteMask
********************************************************************************
*
* Summary:
*  Writes the current mask value assigned to the Status Register.
*
* Parameters:
*  mask:  Value to write into the mask register.
*
* Return:
*  None.
*
*******************************************************************************/
void myStatusReg_WriteMask(uint8 mask) 
{
    #if(myStatusReg_INPUTS < 8u)
    	mask &= ((uint8)(1u << myStatusReg_INPUTS) - 1u);
	#endif /* End myStatusReg_INPUTS < 8u */
    myStatusReg_Status_Mask = mask;
}


/*******************************************************************************
* Function Name: myStatusReg_ReadMask
********************************************************************************
*
* Summary:
*  Reads the current interrupt mask assigned to the Status Register.
*
* Parameters:
*  None.
*
* Return:
*  The value of the interrupt mask of the Status Register.
*
*******************************************************************************/
uint8 myStatusReg_ReadMask(void) 
{
    return myStatusReg_Status_Mask;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
