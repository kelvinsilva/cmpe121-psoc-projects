/*******************************************************************************
* File Name: myADC_PM.c
* Version 3.20
*
* Description:
*  This file provides the power manager source code to the API for the
*  Delta-Sigma ADC Component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "myADC.h"

static myADC_BACKUP_STRUCT myADC_backup =
{
    myADC_DISABLED,
    myADC_CFG1_DEC_CR
};


/*******************************************************************************
* Function Name: myADC_SaveConfig
********************************************************************************
*
* Summary:
*  Save the register configuration which are not retention.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myADC_backup:  This global structure variable is used to store
*  configuration registers which are non retention whenever user wants to go
*  sleep mode by calling Sleep() API.
*
*******************************************************************************/
void myADC_SaveConfig(void) 
{
    myADC_backup.deccr = myADC_DEC_CR_REG;
}


/*******************************************************************************
* Function Name: myADC_RestoreConfig
********************************************************************************
*
* Summary:
*  Restore the register configurations which are not retention.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myADC_backup:  This global structure variable is used to restore
*  configuration registers which are non retention whenever user wants to switch
*  to active power mode by calling Wakeup() API.
*
*******************************************************************************/
void myADC_RestoreConfig(void) 
{
    myADC_DEC_CR_REG = myADC_backup.deccr;
}


/*******************************************************************************
* Function Name: myADC_Sleep
********************************************************************************
*
* Summary:
*  Stops the operation of the block and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myADC_backup:  The structure field 'enableState' is modified
*  depending on the enable state of the block before entering to sleep mode.
*
*******************************************************************************/
void myADC_Sleep(void) 
{
    /* Save ADC enable state */
    if((myADC_ACT_PWR_DEC_EN == (myADC_PWRMGR_DEC_REG & myADC_ACT_PWR_DEC_EN)) &&
       (myADC_ACT_PWR_DSM_EN == (myADC_PWRMGR_DSM_REG & myADC_ACT_PWR_DSM_EN)))
    {
        /* Component is enabled */
        myADC_backup.enableState = myADC_ENABLED;
        if((myADC_DEC_CR_REG & myADC_DEC_START_CONV) != 0u)
        {   
            /* Conversion is started */
            myADC_backup.enableState |= myADC_STARTED;
        }
		
        /* Stop the configuration */
        myADC_Stop();
    }
    else
    {
        /* Component is disabled */
        myADC_backup.enableState = myADC_DISABLED;
    }

    /* Save the user configuration */
    myADC_SaveConfig();
}


/*******************************************************************************
* Function Name: myADC_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and enables the power to the block.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myADC_backup:  The structure field 'enableState' is used to
*  restore the enable state of block after wakeup from sleep mode.
*
*******************************************************************************/
void myADC_Wakeup(void) 
{
    /* Restore the configuration */
    myADC_RestoreConfig();

    /* Enables the component operation */
    if(myADC_backup.enableState != myADC_DISABLED)
    {
        myADC_Enable();
        if((myADC_backup.enableState & myADC_STARTED) != 0u)
        {
            myADC_StartConvert();
        }
    } /* Do nothing if component was disable before */
}


/* [] END OF FILE */
