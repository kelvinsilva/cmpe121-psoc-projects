/*******************************************************************************
* File Name: myVDAC_PM.c  
* Version 1.90
*
* Description:
*  This file provides the power management source code to API for the
*  VDAC8.  
*
* Note:
*  None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "myVDAC.h"

static myVDAC_backupStruct myVDAC_backup;


/*******************************************************************************
* Function Name: myVDAC_SaveConfig
********************************************************************************
* Summary:
*  Save the current user configuration
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void myVDAC_SaveConfig(void) 
{
    if (!((myVDAC_CR1 & myVDAC_SRC_MASK) == myVDAC_SRC_UDB))
    {
        myVDAC_backup.data_value = myVDAC_Data;
    }
}


/*******************************************************************************
* Function Name: myVDAC_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
*******************************************************************************/
void myVDAC_RestoreConfig(void) 
{
    if (!((myVDAC_CR1 & myVDAC_SRC_MASK) == myVDAC_SRC_UDB))
    {
        if((myVDAC_Strobe & myVDAC_STRB_MASK) == myVDAC_STRB_EN)
        {
            myVDAC_Strobe &= (uint8)(~myVDAC_STRB_MASK);
            myVDAC_Data = myVDAC_backup.data_value;
            myVDAC_Strobe |= myVDAC_STRB_EN;
        }
        else
        {
            myVDAC_Data = myVDAC_backup.data_value;
        }
    }
}


/*******************************************************************************
* Function Name: myVDAC_Sleep
********************************************************************************
* Summary:
*  Stop and Save the user configuration
*
* Parameters:  
*  void:  
*
* Return: 
*  void
*
* Global variables:
*  myVDAC_backup.enableState:  Is modified depending on the enable 
*  state  of the block before entering sleep mode.
*
*******************************************************************************/
void myVDAC_Sleep(void) 
{
    /* Save VDAC8's enable state */    
    if(myVDAC_ACT_PWR_EN == (myVDAC_PWRMGR & myVDAC_ACT_PWR_EN))
    {
        /* VDAC8 is enabled */
        myVDAC_backup.enableState = 1u;
    }
    else
    {
        /* VDAC8 is disabled */
        myVDAC_backup.enableState = 0u;
    }
    
    myVDAC_Stop();
    myVDAC_SaveConfig();
}


/*******************************************************************************
* Function Name: myVDAC_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  myVDAC_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void myVDAC_Wakeup(void) 
{
    myVDAC_RestoreConfig();
    
    if(myVDAC_backup.enableState == 1u)
    {
        /* Enable VDAC8's operation */
        myVDAC_Enable();

        /* Restore the data register */
        myVDAC_SetValue(myVDAC_Data);
    } /* Do nothing if VDAC8 was disabled before */    
}


/* [] END OF FILE */
