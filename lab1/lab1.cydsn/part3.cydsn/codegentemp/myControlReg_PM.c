/*******************************************************************************
* File Name: myControlReg_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "myControlReg.h"

/* Check for removal by optimization */
#if !defined(myControlReg_Sync_ctrl_reg__REMOVED)

static myControlReg_BACKUP_STRUCT  myControlReg_backup = {0u};

    
/*******************************************************************************
* Function Name: myControlReg_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void myControlReg_SaveConfig(void) 
{
    myControlReg_backup.controlState = myControlReg_Control;
}


/*******************************************************************************
* Function Name: myControlReg_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void myControlReg_RestoreConfig(void) 
{
     myControlReg_Control = myControlReg_backup.controlState;
}


/*******************************************************************************
* Function Name: myControlReg_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void myControlReg_Sleep(void) 
{
    myControlReg_SaveConfig();
}


/*******************************************************************************
* Function Name: myControlReg_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void myControlReg_Wakeup(void)  
{
    myControlReg_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
