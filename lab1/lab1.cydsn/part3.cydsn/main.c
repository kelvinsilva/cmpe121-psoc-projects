/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

int main(void)
{
   

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    myPWM_Start();
    myPWM_Enable();
   
    
    myADC_Start();
    
    myTimer_Start();
    myTimer_Enable();
    
    myCounter_Start();
    myCounter_Enable();
    
    myInterrupt_Start();
    myInterrupt_Enable();
    
    myVDAC_Start();
    myVDAC_Enable();
    
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    for(;;)
    {
        uint16_t varAdc = myADC_Read32() / 65;
        /* Place your application code here. */
        myPWM_WritePeriod( varAdc);
        myPWM_WriteCompare( varAdc / 2);
        
    }
}

/* [] END OF FILE */
