/*******************************************************************************
* File Name: myVDAC.c  
* Version 1.90
*
* Description:
*  This file provides the source code to the API for the 8-bit Voltage DAC 
*  (VDAC8) User Module.
*
* Note:
*  Any unusual or non-standard behavior should be noted here. Other-
*  wise, this section should remain blank.
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "myVDAC.h"

#if (CY_PSOC5A)
#include <CyLib.h>
#endif /* CY_PSOC5A */

uint8 myVDAC_initVar = 0u;

#if (CY_PSOC5A)
    static uint8 myVDAC_restoreVal = 0u;
#endif /* CY_PSOC5A */

#if (CY_PSOC5A)
    static myVDAC_backupStruct myVDAC_backup;
#endif /* CY_PSOC5A */


/*******************************************************************************
* Function Name: myVDAC_Init
********************************************************************************
* Summary:
*  Initialize to the schematic state.
* 
* Parameters:
*  void:
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_Init(void) 
{
    myVDAC_CR0 = (myVDAC_MODE_V );

    /* Set default data source */
    #if(myVDAC_DEFAULT_DATA_SRC != 0 )
        myVDAC_CR1 = (myVDAC_DEFAULT_CNTL | myVDAC_DACBUS_ENABLE) ;
    #else
        myVDAC_CR1 = (myVDAC_DEFAULT_CNTL | myVDAC_DACBUS_DISABLE) ;
    #endif /* (myVDAC_DEFAULT_DATA_SRC != 0 ) */

    /* Set default strobe mode */
    #if(myVDAC_DEFAULT_STRB != 0)
        myVDAC_Strobe |= myVDAC_STRB_EN ;
    #endif/* (myVDAC_DEFAULT_STRB != 0) */

    /* Set default range */
    myVDAC_SetRange(myVDAC_DEFAULT_RANGE); 

    /* Set default speed */
    myVDAC_SetSpeed(myVDAC_DEFAULT_SPEED);
}


/*******************************************************************************
* Function Name: myVDAC_Enable
********************************************************************************
* Summary:
*  Enable the VDAC8
* 
* Parameters:
*  void
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_Enable(void) 
{
    myVDAC_PWRMGR |= myVDAC_ACT_PWR_EN;
    myVDAC_STBY_PWRMGR |= myVDAC_STBY_PWR_EN;

    /*This is to restore the value of register CR0 ,
    which is modified  in Stop API , this prevents misbehaviour of VDAC */
    #if (CY_PSOC5A)
        if(myVDAC_restoreVal == 1u) 
        {
             myVDAC_CR0 = myVDAC_backup.data_value;
             myVDAC_restoreVal = 0u;
        }
    #endif /* CY_PSOC5A */
}


/*******************************************************************************
* Function Name: myVDAC_Start
********************************************************************************
*
* Summary:
*  The start function initializes the voltage DAC with the default values, 
*  and sets the power to the given level.  A power level of 0, is the same as
*  executing the stop function.
*
* Parameters:
*  Power: Sets power level between off (0) and (3) high power
*
* Return:
*  void 
*
* Global variables:
*  myVDAC_initVar: Is modified when this function is called for the 
*  first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void myVDAC_Start(void)  
{
    /* Hardware initiazation only needs to occure the first time */
    if(myVDAC_initVar == 0u)
    { 
        myVDAC_Init();
        myVDAC_initVar = 1u;
    }

    /* Enable power to DAC */
    myVDAC_Enable();

    /* Set default value */
    myVDAC_SetValue(myVDAC_DEFAULT_DATA); 
}


/*******************************************************************************
* Function Name: myVDAC_Stop
********************************************************************************
*
* Summary:
*  Powers down DAC to lowest power state.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_Stop(void) 
{
    /* Disble power to DAC */
    myVDAC_PWRMGR &= (uint8)(~myVDAC_ACT_PWR_EN);
    myVDAC_STBY_PWRMGR &= (uint8)(~myVDAC_STBY_PWR_EN);

    /* This is a work around for PSoC5A  ,
    this sets VDAC to current mode with output off */
    #if (CY_PSOC5A)
        myVDAC_backup.data_value = myVDAC_CR0;
        myVDAC_CR0 = myVDAC_CUR_MODE_OUT_OFF;
        myVDAC_restoreVal = 1u;
    #endif /* CY_PSOC5A */
}


/*******************************************************************************
* Function Name: myVDAC_SetSpeed
********************************************************************************
*
* Summary:
*  Set DAC speed
*
* Parameters:
*  power: Sets speed value
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_SetSpeed(uint8 speed) 
{
    /* Clear power mask then write in new value */
    myVDAC_CR0 &= (uint8)(~myVDAC_HS_MASK);
    myVDAC_CR0 |=  (speed & myVDAC_HS_MASK);
}


/*******************************************************************************
* Function Name: myVDAC_SetRange
********************************************************************************
*
* Summary:
*  Set one of three current ranges.
*
* Parameters:
*  Range: Sets one of Three valid ranges.
*
* Return:
*  void 
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_SetRange(uint8 range) 
{
    myVDAC_CR0 &= (uint8)(~myVDAC_RANGE_MASK);      /* Clear existing mode */
    myVDAC_CR0 |= (range & myVDAC_RANGE_MASK);      /*  Set Range  */
    myVDAC_DacTrim();
}


/*******************************************************************************
* Function Name: myVDAC_SetValue
********************************************************************************
*
* Summary:
*  Set 8-bit DAC value
*
* Parameters:  
*  value:  Sets DAC value between 0 and 255.
*
* Return: 
*  void 
*
* Theory: 
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_SetValue(uint8 value) 
{
    #if (CY_PSOC5A)
        uint8 myVDAC_intrStatus = CyEnterCriticalSection();
    #endif /* CY_PSOC5A */

    myVDAC_Data = value;                /*  Set Value  */

    /* PSOC5A requires a double write */
    /* Exit Critical Section */
    #if (CY_PSOC5A)
        myVDAC_Data = value;
        CyExitCriticalSection(myVDAC_intrStatus);
    #endif /* CY_PSOC5A */
}


/*******************************************************************************
* Function Name: myVDAC_DacTrim
********************************************************************************
*
* Summary:
*  Set the trim value for the given range.
*
* Parameters:
*  range:  1V or 4V range.  See constants.
*
* Return:
*  void
*
* Theory: 
*
* Side Effects:
*
*******************************************************************************/
void myVDAC_DacTrim(void) 
{
    uint8 mode;

    mode = (uint8)((myVDAC_CR0 & myVDAC_RANGE_MASK) >> 2) + myVDAC_TRIM_M7_1V_RNG_OFFSET;
    myVDAC_TR = CY_GET_XTND_REG8((uint8 *)(myVDAC_DAC_TRIM_BASE + mode));
}


/* [] END OF FILE */
