/*******************************************************************************
* File Name: myVDAC.h  
* Version 1.90
*
*  Description:
*    This file contains the function prototypes and constants used in
*    the 8-bit Voltage DAC (vDAC8) User Module.
*
*   Note:
*     
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_VDAC8_myVDAC_H) 
#define CY_VDAC8_myVDAC_H

#include "cytypes.h"
#include "cyfitter.h"

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component VDAC8_v1_90 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */


/***************************************
*       Type defines
***************************************/

/* Sleep Mode API Support */
typedef struct
{
    uint8 enableState; 
    uint8 data_value;
}myVDAC_backupStruct;

/* component init state */
extern uint8 myVDAC_initVar;


/***************************************
*        Function Prototypes 
***************************************/

void myVDAC_Start(void)           ;
void myVDAC_Stop(void)            ;
void myVDAC_SetSpeed(uint8 speed) ;
void myVDAC_SetRange(uint8 range) ;
void myVDAC_SetValue(uint8 value) ;
void myVDAC_DacTrim(void)         ;
void myVDAC_Init(void)            ;
void myVDAC_Enable(void)          ;
void myVDAC_SaveConfig(void)      ;
void myVDAC_RestoreConfig(void)   ;
void myVDAC_Sleep(void)           ;
void myVDAC_Wakeup(void)          ;


/***************************************
*            API Constants
***************************************/

/* SetRange constants */

#define myVDAC_RANGE_1V       0x00u
#define myVDAC_RANGE_4V       0x04u


/* Power setting for Start API  */
#define myVDAC_LOWSPEED       0x00u
#define myVDAC_HIGHSPEED      0x02u


/***************************************
*  Initialization Parameter Constants
***************************************/

 /* Default DAC range */
#define myVDAC_DEFAULT_RANGE    4u
 /* Default DAC speed */
#define myVDAC_DEFAULT_SPEED    0u
 /* Default Control */
#define myVDAC_DEFAULT_CNTL      0x00u
/* Default Strobe mode */
#define myVDAC_DEFAULT_STRB     0u
 /* Initial DAC value */
#define myVDAC_DEFAULT_DATA     100u
 /* Default Data Source */
#define myVDAC_DEFAULT_DATA_SRC 0u


/***************************************
*              Registers        
***************************************/
#define myVDAC_CR0_REG            (* (reg8 *) myVDAC_viDAC8__CR0 )
#define myVDAC_CR0_PTR            (  (reg8 *) myVDAC_viDAC8__CR0 )
#define myVDAC_CR1_REG            (* (reg8 *) myVDAC_viDAC8__CR1 )
#define myVDAC_CR1_PTR            (  (reg8 *) myVDAC_viDAC8__CR1 )
#define myVDAC_Data_REG           (* (reg8 *) myVDAC_viDAC8__D )
#define myVDAC_Data_PTR           (  (reg8 *) myVDAC_viDAC8__D )
#define myVDAC_Strobe_REG         (* (reg8 *) myVDAC_viDAC8__STROBE )
#define myVDAC_Strobe_PTR         (  (reg8 *) myVDAC_viDAC8__STROBE )
#define myVDAC_SW0_REG            (* (reg8 *) myVDAC_viDAC8__SW0 )
#define myVDAC_SW0_PTR            (  (reg8 *) myVDAC_viDAC8__SW0 )
#define myVDAC_SW2_REG            (* (reg8 *) myVDAC_viDAC8__SW2 )
#define myVDAC_SW2_PTR            (  (reg8 *) myVDAC_viDAC8__SW2 )
#define myVDAC_SW3_REG            (* (reg8 *) myVDAC_viDAC8__SW3 )
#define myVDAC_SW3_PTR            (  (reg8 *) myVDAC_viDAC8__SW3 )
#define myVDAC_SW4_REG            (* (reg8 *) myVDAC_viDAC8__SW4 )
#define myVDAC_SW4_PTR            (  (reg8 *) myVDAC_viDAC8__SW4 )
#define myVDAC_TR_REG             (* (reg8 *) myVDAC_viDAC8__TR )
#define myVDAC_TR_PTR             (  (reg8 *) myVDAC_viDAC8__TR )
/* Power manager */
#define myVDAC_PWRMGR_REG         (* (reg8 *) myVDAC_viDAC8__PM_ACT_CFG )
#define myVDAC_PWRMGR_PTR         (  (reg8 *) myVDAC_viDAC8__PM_ACT_CFG )
  /* Standby Power manager */
#define myVDAC_STBY_PWRMGR_REG    (* (reg8 *) myVDAC_viDAC8__PM_STBY_CFG )
#define myVDAC_STBY_PWRMGR_PTR    (  (reg8 *) myVDAC_viDAC8__PM_STBY_CFG )

/***************************************
*  Registers definitions
* for backward capability        
***************************************/
#define myVDAC_CR0         (* (reg8 *) myVDAC_viDAC8__CR0 )
#define myVDAC_CR1         (* (reg8 *) myVDAC_viDAC8__CR1 )
#define myVDAC_Data        (* (reg8 *) myVDAC_viDAC8__D )
#define myVDAC_Data_PTR    (  (reg8 *) myVDAC_viDAC8__D )
#define myVDAC_Strobe      (* (reg8 *) myVDAC_viDAC8__STROBE )
#define myVDAC_SW0         (* (reg8 *) myVDAC_viDAC8__SW0 )
#define myVDAC_SW2         (* (reg8 *) myVDAC_viDAC8__SW2 )
#define myVDAC_SW3         (* (reg8 *) myVDAC_viDAC8__SW3 )
#define myVDAC_SW4         (* (reg8 *) myVDAC_viDAC8__SW4 )
#define myVDAC_TR          (* (reg8 *) myVDAC_viDAC8__TR )
/* Power manager */
#define myVDAC_PWRMGR      (* (reg8 *) myVDAC_viDAC8__PM_ACT_CFG )
  /* Standby Power manager */
#define myVDAC_STBY_PWRMGR (* (reg8 *) myVDAC_viDAC8__PM_STBY_CFG )


/***************************************
*         Register Constants       
***************************************/

/* CR0 vDac Control Register 0 definitions */

/* Bit Field  DAC_HS_MODE                  */
#define myVDAC_HS_MASK        0x02u
#define myVDAC_HS_LOWPOWER    0x00u
#define myVDAC_HS_HIGHSPEED   0x02u

/* Bit Field  DAC_MODE                  */
#define myVDAC_MODE_MASK      0x10u
#define myVDAC_MODE_V         0x00u
#define myVDAC_MODE_I         0x10u

/* Bit Field  DAC_RANGE                  */
#define myVDAC_RANGE_MASK     0x0Cu
#define myVDAC_RANGE_0        0x00u
#define myVDAC_RANGE_1        0x04u

/* CR1 iDac Control Register 1 definitions */

/* Bit Field  DAC_MX_DATA                  */
#define myVDAC_SRC_MASK       0x20u
#define myVDAC_SRC_REG        0x00u
#define myVDAC_SRC_UDB        0x20u

/* This bit enable reset from UDB array      */
#define myVDAC_RESET_MASK     0x10u
#define myVDAC_RESET_ENABLE   0x10u
#define myVDAC_RESET_DISABLE  0x00u

/* This bit enables data from DAC bus      */
#define myVDAC_DACBUS_MASK     0x20u
#define myVDAC_DACBUS_ENABLE   0x20u
#define myVDAC_DACBUS_DISABLE  0x00u

/* DAC STROBE Strobe Control Register definitions */

/* Bit Field  DAC_MX_STROBE                  */
#define myVDAC_STRB_MASK     0x08u
#define myVDAC_STRB_EN       0x08u
#define myVDAC_STRB_DIS      0x00u

/* PM_ACT_CFG (Active Power Mode CFG Register)     */ 
#define myVDAC_ACT_PWR_EN   myVDAC_viDAC8__PM_ACT_MSK
  /* Standby Power enable mask */
#define myVDAC_STBY_PWR_EN  myVDAC_viDAC8__PM_STBY_MSK


/*******************************************************************************
*              Trim    
* Note - VDAC trim values are stored in the "Customer Table" area in * Row 1 of
*the Hidden Flash.  There are 8 bytes of trim data for each VDAC block.
* The values are:
*       I Gain offset, min range, Sourcing
*       I Gain offset, min range, Sinking
*       I Gain offset, med range, Sourcing
*       I Gain offset, med range, Sinking
*       I Gain offset, max range, Sourcing
*       I Gain offset, max range, Sinking
*       V Gain offset, 1V range
*       V Gain offset, 4V range
*
* The data set for the 4 VDACs are arranged using a left side/right side
* approach:
*   Left 0, Left 1, Right 0, Right 1.
* When mapped to the VDAC0 thru VDAC3 as:
*   VDAC 0, VDAC 2, VDAC 1, VDAC 3
*******************************************************************************/
#define myVDAC_TRIM_M7_1V_RNG_OFFSET  0x06u
#define myVDAC_TRIM_M8_4V_RNG_OFFSET  0x07u
/*Constatnt to set DAC in current mode and turnoff output */
#define myVDAC_CUR_MODE_OUT_OFF       0x1Eu 
#define myVDAC_DAC_TRIM_BASE          (myVDAC_viDAC8__TRIM__M1)

#endif /* CY_VDAC8_myVDAC_H  */


/* [] END OF FILE */


