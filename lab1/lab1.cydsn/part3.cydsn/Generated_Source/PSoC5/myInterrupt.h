/*******************************************************************************
* File Name: myInterrupt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_myInterrupt_H)
#define CY_ISR_myInterrupt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void myInterrupt_Start(void);
void myInterrupt_StartEx(cyisraddress address);
void myInterrupt_Stop(void);

CY_ISR_PROTO(myInterrupt_Interrupt);

void myInterrupt_SetVector(cyisraddress address);
cyisraddress myInterrupt_GetVector(void);

void myInterrupt_SetPriority(uint8 priority);
uint8 myInterrupt_GetPriority(void);

void myInterrupt_Enable(void);
uint8 myInterrupt_GetState(void);
void myInterrupt_Disable(void);

void myInterrupt_SetPending(void);
void myInterrupt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the myInterrupt ISR. */
#define myInterrupt_INTC_VECTOR            ((reg32 *) myInterrupt__INTC_VECT)

/* Address of the myInterrupt ISR priority. */
#define myInterrupt_INTC_PRIOR             ((reg8 *) myInterrupt__INTC_PRIOR_REG)

/* Priority of the myInterrupt interrupt. */
#define myInterrupt_INTC_PRIOR_NUMBER      myInterrupt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable myInterrupt interrupt. */
#define myInterrupt_INTC_SET_EN            ((reg32 *) myInterrupt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the myInterrupt interrupt. */
#define myInterrupt_INTC_CLR_EN            ((reg32 *) myInterrupt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the myInterrupt interrupt state to pending. */
#define myInterrupt_INTC_SET_PD            ((reg32 *) myInterrupt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the myInterrupt interrupt. */
#define myInterrupt_INTC_CLR_PD            ((reg32 *) myInterrupt__INTC_CLR_PD_REG)


#endif /* CY_ISR_myInterrupt_H */


/* [] END OF FILE */
