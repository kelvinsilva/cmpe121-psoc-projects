/*******************************************************************************
* File Name: myTimer_PM.c
* Version 2.70
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "myTimer.h"

static myTimer_backupStruct myTimer_backup;


/*******************************************************************************
* Function Name: myTimer_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  myTimer_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void myTimer_SaveConfig(void) 
{
    #if (!myTimer_UsingFixedFunction)
        myTimer_backup.TimerUdb = myTimer_ReadCounter();
        myTimer_backup.InterruptMaskValue = myTimer_STATUS_MASK;
        #if (myTimer_UsingHWCaptureCounter)
            myTimer_backup.TimerCaptureCounter = myTimer_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!myTimer_UDB_CONTROL_REG_REMOVED)
            myTimer_backup.TimerControlRegister = myTimer_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: myTimer_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  myTimer_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void myTimer_RestoreConfig(void) 
{   
    #if (!myTimer_UsingFixedFunction)

        myTimer_WriteCounter(myTimer_backup.TimerUdb);
        myTimer_STATUS_MASK =myTimer_backup.InterruptMaskValue;
        #if (myTimer_UsingHWCaptureCounter)
            myTimer_SetCaptureCount(myTimer_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!myTimer_UDB_CONTROL_REG_REMOVED)
            myTimer_WriteControlRegister(myTimer_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: myTimer_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  myTimer_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void myTimer_Sleep(void) 
{
    #if(!myTimer_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(myTimer_CTRL_ENABLE == (myTimer_CONTROL & myTimer_CTRL_ENABLE))
        {
            /* Timer is enabled */
            myTimer_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            myTimer_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    myTimer_Stop();
    myTimer_SaveConfig();
}


/*******************************************************************************
* Function Name: myTimer_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  myTimer_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void myTimer_Wakeup(void) 
{
    myTimer_RestoreConfig();
    #if(!myTimer_UDB_CONTROL_REG_REMOVED)
        if(myTimer_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                myTimer_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
