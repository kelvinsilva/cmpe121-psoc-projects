/*******************************************************************************
* File Name: myADC_AMux.h
* Version 1.80
*
*  Description:
*    This file contains the constants and function prototypes for the Analog
*    Multiplexer User Module AMux.
*
*   Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_AMUX_myADC_AMux_H)
#define CY_AMUX_myADC_AMux_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cyfitter_cfg.h"


/***************************************
*        Function Prototypes
***************************************/

void myADC_AMux_Start(void) ;
#define myADC_AMux_Init() myADC_AMux_Start()
void myADC_AMux_FastSelect(uint8 channel) ;
/* The Stop, Select, Connect, Disconnect and DisconnectAll functions are declared elsewhere */
/* void myADC_AMux_Stop(void); */
/* void myADC_AMux_Select(uint8 channel); */
/* void myADC_AMux_Connect(uint8 channel); */
/* void myADC_AMux_Disconnect(uint8 channel); */
/* void myADC_AMux_DisconnectAll(void) */


/***************************************
*         Parameter Constants
***************************************/

#define myADC_AMux_CHANNELS  2u
#define myADC_AMux_MUXTYPE   1
#define myADC_AMux_ATMOSTONE 0

/***************************************
*             API Constants
***************************************/

#define myADC_AMux_NULL_CHANNEL 0xFFu
#define myADC_AMux_MUX_SINGLE   1
#define myADC_AMux_MUX_DIFF     2


/***************************************
*        Conditional Functions
***************************************/

#if myADC_AMux_MUXTYPE == myADC_AMux_MUX_SINGLE
# if !myADC_AMux_ATMOSTONE
#  define myADC_AMux_Connect(channel) myADC_AMux_Set(channel)
# endif
# define myADC_AMux_Disconnect(channel) myADC_AMux_Unset(channel)
#else
# if !myADC_AMux_ATMOSTONE
void myADC_AMux_Connect(uint8 channel) ;
# endif
void myADC_AMux_Disconnect(uint8 channel) ;
#endif

#if myADC_AMux_ATMOSTONE
# define myADC_AMux_Stop() myADC_AMux_DisconnectAll()
# define myADC_AMux_Select(channel) myADC_AMux_FastSelect(channel)
void myADC_AMux_DisconnectAll(void) ;
#else
# define myADC_AMux_Stop() myADC_AMux_Start()
void myADC_AMux_Select(uint8 channel) ;
# define myADC_AMux_DisconnectAll() myADC_AMux_Start()
#endif

#endif /* CY_AMUX_myADC_AMux_H */


/* [] END OF FILE */
