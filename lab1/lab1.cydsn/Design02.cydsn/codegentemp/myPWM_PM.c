/*******************************************************************************
* File Name: myPWM_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "myPWM.h"

static myPWM_backupStruct myPWM_backup;


/*******************************************************************************
* Function Name: myPWM_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myPWM_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void myPWM_SaveConfig(void) 
{

    #if(!myPWM_UsingFixedFunction)
        #if(!myPWM_PWMModeIsCenterAligned)
            myPWM_backup.PWMPeriod = myPWM_ReadPeriod();
        #endif /* (!myPWM_PWMModeIsCenterAligned) */
        myPWM_backup.PWMUdb = myPWM_ReadCounter();
        #if (myPWM_UseStatus)
            myPWM_backup.InterruptMaskValue = myPWM_STATUS_MASK;
        #endif /* (myPWM_UseStatus) */

        #if(myPWM_DeadBandMode == myPWM__B_PWM__DBM_256_CLOCKS || \
            myPWM_DeadBandMode == myPWM__B_PWM__DBM_2_4_CLOCKS)
            myPWM_backup.PWMdeadBandValue = myPWM_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(myPWM_KillModeMinTime)
             myPWM_backup.PWMKillCounterPeriod = myPWM_ReadKillTime();
        #endif /* (myPWM_KillModeMinTime) */

        #if(myPWM_UseControl)
            myPWM_backup.PWMControlRegister = myPWM_ReadControlRegister();
        #endif /* (myPWM_UseControl) */
    #endif  /* (!myPWM_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: myPWM_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myPWM_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void myPWM_RestoreConfig(void) 
{
        #if(!myPWM_UsingFixedFunction)
            #if(!myPWM_PWMModeIsCenterAligned)
                myPWM_WritePeriod(myPWM_backup.PWMPeriod);
            #endif /* (!myPWM_PWMModeIsCenterAligned) */

            myPWM_WriteCounter(myPWM_backup.PWMUdb);

            #if (myPWM_UseStatus)
                myPWM_STATUS_MASK = myPWM_backup.InterruptMaskValue;
            #endif /* (myPWM_UseStatus) */

            #if(myPWM_DeadBandMode == myPWM__B_PWM__DBM_256_CLOCKS || \
                myPWM_DeadBandMode == myPWM__B_PWM__DBM_2_4_CLOCKS)
                myPWM_WriteDeadTime(myPWM_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(myPWM_KillModeMinTime)
                myPWM_WriteKillTime(myPWM_backup.PWMKillCounterPeriod);
            #endif /* (myPWM_KillModeMinTime) */

            #if(myPWM_UseControl)
                myPWM_WriteControlRegister(myPWM_backup.PWMControlRegister);
            #endif /* (myPWM_UseControl) */
        #endif  /* (!myPWM_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: myPWM_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myPWM_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void myPWM_Sleep(void) 
{
    #if(myPWM_UseControl)
        if(myPWM_CTRL_ENABLE == (myPWM_CONTROL & myPWM_CTRL_ENABLE))
        {
            /*Component is enabled */
            myPWM_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            myPWM_backup.PWMEnableState = 0u;
        }
    #endif /* (myPWM_UseControl) */

    /* Stop component */
    myPWM_Stop();

    /* Save registers configuration */
    myPWM_SaveConfig();
}


/*******************************************************************************
* Function Name: myPWM_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  myPWM_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void myPWM_Wakeup(void) 
{
     /* Restore registers values */
    myPWM_RestoreConfig();

    if(myPWM_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        myPWM_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
