/*******************************************************************************
* File Name: myPWM.h
* Version 3.30
*
* Description:
*  Contains the prototypes and constants for the functions available to the
*  PWM user module.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PWM_myPWM_H)
#define CY_PWM_myPWM_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 myPWM_initVar;


/***************************************
* Conditional Compilation Parameters
***************************************/
#define myPWM_Resolution                     (16u)
#define myPWM_UsingFixedFunction             (0u)
#define myPWM_DeadBandMode                   (0u)
#define myPWM_KillModeMinTime                (0u)
#define myPWM_KillMode                       (0u)
#define myPWM_PWMMode                        (0u)
#define myPWM_PWMModeIsCenterAligned         (0u)
#define myPWM_DeadBandUsed                   (0u)
#define myPWM_DeadBand2_4                    (0u)

#if !defined(myPWM_PWMUDB_genblk8_stsreg__REMOVED)
    #define myPWM_UseStatus                  (1u)
#else
    #define myPWM_UseStatus                  (0u)
#endif /* !defined(myPWM_PWMUDB_genblk8_stsreg__REMOVED) */

#if !defined(myPWM_PWMUDB_genblk1_ctrlreg__REMOVED)
    #define myPWM_UseControl                 (1u)
#else
    #define myPWM_UseControl                 (0u)
#endif /* !defined(myPWM_PWMUDB_genblk1_ctrlreg__REMOVED) */

#define myPWM_UseOneCompareMode              (1u)
#define myPWM_MinimumKillTime                (1u)
#define myPWM_EnableMode                     (0u)

#define myPWM_CompareMode1SW                 (0u)
#define myPWM_CompareMode2SW                 (0u)

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component PWM_v3_30 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */

/* Use Kill Mode Enumerated Types */
#define myPWM__B_PWM__DISABLED 0
#define myPWM__B_PWM__ASYNCHRONOUS 1
#define myPWM__B_PWM__SINGLECYCLE 2
#define myPWM__B_PWM__LATCHED 3
#define myPWM__B_PWM__MINTIME 4


/* Use Dead Band Mode Enumerated Types */
#define myPWM__B_PWM__DBMDISABLED 0
#define myPWM__B_PWM__DBM_2_4_CLOCKS 1
#define myPWM__B_PWM__DBM_256_CLOCKS 2


/* Used PWM Mode Enumerated Types */
#define myPWM__B_PWM__ONE_OUTPUT 0
#define myPWM__B_PWM__TWO_OUTPUTS 1
#define myPWM__B_PWM__DUAL_EDGE 2
#define myPWM__B_PWM__CENTER_ALIGN 3
#define myPWM__B_PWM__DITHER 5
#define myPWM__B_PWM__HARDWARESELECT 4


/* Used PWM Compare Mode Enumerated Types */
#define myPWM__B_PWM__LESS_THAN 1
#define myPWM__B_PWM__LESS_THAN_OR_EQUAL 2
#define myPWM__B_PWM__GREATER_THAN 3
#define myPWM__B_PWM__GREATER_THAN_OR_EQUAL_TO 4
#define myPWM__B_PWM__EQUAL 0
#define myPWM__B_PWM__FIRMWARE 5



/***************************************
* Data Struct Definition
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for PWM Component
 *************************************************************************/
typedef struct
{

    uint8 PWMEnableState;

    #if(!myPWM_UsingFixedFunction)
        uint16 PWMUdb;               /* PWM Current Counter value  */
        #if(!myPWM_PWMModeIsCenterAligned)
            uint16 PWMPeriod;
        #endif /* (!myPWM_PWMModeIsCenterAligned) */
        #if (myPWM_UseStatus)
            uint8 InterruptMaskValue;   /* PWM Current Interrupt Mask */
        #endif /* (myPWM_UseStatus) */

        /* Backup for Deadband parameters */
        #if(myPWM_DeadBandMode == myPWM__B_PWM__DBM_256_CLOCKS || \
            myPWM_DeadBandMode == myPWM__B_PWM__DBM_2_4_CLOCKS)
            uint8 PWMdeadBandValue; /* Dead Band Counter Current Value */
        #endif /* deadband count is either 2-4 clocks or 256 clocks */

        /* Backup Kill Mode Counter*/
        #if(myPWM_KillModeMinTime)
            uint8 PWMKillCounterPeriod; /* Kill Mode period value */
        #endif /* (myPWM_KillModeMinTime) */

        /* Backup control register */
        #if(myPWM_UseControl)
            uint8 PWMControlRegister; /* PWM Control Register value */
        #endif /* (myPWM_UseControl) */

    #endif /* (!myPWM_UsingFixedFunction) */

}myPWM_backupStruct;


/***************************************
*        Function Prototypes
 **************************************/

void    myPWM_Start(void) ;
void    myPWM_Stop(void) ;

#if (myPWM_UseStatus || myPWM_UsingFixedFunction)
    void  myPWM_SetInterruptMode(uint8 interruptMode) ;
    uint8 myPWM_ReadStatusRegister(void) ;
#endif /* (myPWM_UseStatus || myPWM_UsingFixedFunction) */

#define myPWM_GetInterruptSource() myPWM_ReadStatusRegister()

#if (myPWM_UseControl)
    uint8 myPWM_ReadControlRegister(void) ;
    void  myPWM_WriteControlRegister(uint8 control)
          ;
#endif /* (myPWM_UseControl) */

#if (myPWM_UseOneCompareMode)
   #if (myPWM_CompareMode1SW)
       void    myPWM_SetCompareMode(uint8 comparemode)
               ;
   #endif /* (myPWM_CompareMode1SW) */
#else
    #if (myPWM_CompareMode1SW)
        void    myPWM_SetCompareMode1(uint8 comparemode)
                ;
    #endif /* (myPWM_CompareMode1SW) */
    #if (myPWM_CompareMode2SW)
        void    myPWM_SetCompareMode2(uint8 comparemode)
                ;
    #endif /* (myPWM_CompareMode2SW) */
#endif /* (myPWM_UseOneCompareMode) */

#if (!myPWM_UsingFixedFunction)
    uint16   myPWM_ReadCounter(void) ;
    uint16 myPWM_ReadCapture(void) ;

    #if (myPWM_UseStatus)
            void myPWM_ClearFIFO(void) ;
    #endif /* (myPWM_UseStatus) */

    void    myPWM_WriteCounter(uint16 counter)
            ;
#endif /* (!myPWM_UsingFixedFunction) */

void    myPWM_WritePeriod(uint16 period)
        ;
uint16 myPWM_ReadPeriod(void) ;

#if (myPWM_UseOneCompareMode)
    void    myPWM_WriteCompare(uint16 compare)
            ;
    uint16 myPWM_ReadCompare(void) ;
#else
    void    myPWM_WriteCompare1(uint16 compare)
            ;
    uint16 myPWM_ReadCompare1(void) ;
    void    myPWM_WriteCompare2(uint16 compare)
            ;
    uint16 myPWM_ReadCompare2(void) ;
#endif /* (myPWM_UseOneCompareMode) */


#if (myPWM_DeadBandUsed)
    void    myPWM_WriteDeadTime(uint8 deadtime) ;
    uint8   myPWM_ReadDeadTime(void) ;
#endif /* (myPWM_DeadBandUsed) */

#if ( myPWM_KillModeMinTime)
    void myPWM_WriteKillTime(uint8 killtime) ;
    uint8 myPWM_ReadKillTime(void) ;
#endif /* ( myPWM_KillModeMinTime) */

void myPWM_Init(void) ;
void myPWM_Enable(void) ;
void myPWM_Sleep(void) ;
void myPWM_Wakeup(void) ;
void myPWM_SaveConfig(void) ;
void myPWM_RestoreConfig(void) ;


/***************************************
*         Initialization Values
**************************************/
#define myPWM_INIT_PERIOD_VALUE          (999u)
#define myPWM_INIT_COMPARE_VALUE1        (324u)
#define myPWM_INIT_COMPARE_VALUE2        (325u)
#define myPWM_INIT_INTERRUPTS_MODE       (uint8)(((uint8)(0u <<   \
                                                    myPWM_STATUS_TC_INT_EN_MASK_SHIFT)) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    myPWM_STATUS_CMP2_INT_EN_MASK_SHIFT)) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    myPWM_STATUS_CMP1_INT_EN_MASK_SHIFT )) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    myPWM_STATUS_KILL_INT_EN_MASK_SHIFT )))
#define myPWM_DEFAULT_COMPARE2_MODE      (uint8)((uint8)1u <<  myPWM_CTRL_CMPMODE2_SHIFT)
#define myPWM_DEFAULT_COMPARE1_MODE      (uint8)((uint8)1u <<  myPWM_CTRL_CMPMODE1_SHIFT)
#define myPWM_INIT_DEAD_TIME             (1u)


/********************************
*         Registers
******************************** */

#if (myPWM_UsingFixedFunction)
   #define myPWM_PERIOD_LSB              (*(reg16 *) myPWM_PWMHW__PER0)
   #define myPWM_PERIOD_LSB_PTR          ( (reg16 *) myPWM_PWMHW__PER0)
   #define myPWM_COMPARE1_LSB            (*(reg16 *) myPWM_PWMHW__CNT_CMP0)
   #define myPWM_COMPARE1_LSB_PTR        ( (reg16 *) myPWM_PWMHW__CNT_CMP0)
   #define myPWM_COMPARE2_LSB            (0x00u)
   #define myPWM_COMPARE2_LSB_PTR        (0x00u)
   #define myPWM_COUNTER_LSB             (*(reg16 *) myPWM_PWMHW__CNT_CMP0)
   #define myPWM_COUNTER_LSB_PTR         ( (reg16 *) myPWM_PWMHW__CNT_CMP0)
   #define myPWM_CAPTURE_LSB             (*(reg16 *) myPWM_PWMHW__CAP0)
   #define myPWM_CAPTURE_LSB_PTR         ( (reg16 *) myPWM_PWMHW__CAP0)
   #define myPWM_RT1                     (*(reg8 *)  myPWM_PWMHW__RT1)
   #define myPWM_RT1_PTR                 ( (reg8 *)  myPWM_PWMHW__RT1)

#else
   #if (myPWM_Resolution == 8u) /* 8bit - PWM */

       #if(myPWM_PWMModeIsCenterAligned)
           #define myPWM_PERIOD_LSB      (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
           #define myPWM_PERIOD_LSB_PTR  ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
       #else
           #define myPWM_PERIOD_LSB      (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__F0_REG)
           #define myPWM_PERIOD_LSB_PTR  ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__F0_REG)
       #endif /* (myPWM_PWMModeIsCenterAligned) */

       #define myPWM_COMPARE1_LSB        (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__D0_REG)
       #define myPWM_COMPARE1_LSB_PTR    ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__D0_REG)
       #define myPWM_COMPARE2_LSB        (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
       #define myPWM_COMPARE2_LSB_PTR    ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
       #define myPWM_COUNTERCAP_LSB      (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__A1_REG)
       #define myPWM_COUNTERCAP_LSB_PTR  ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__A1_REG)
       #define myPWM_COUNTER_LSB         (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__A0_REG)
       #define myPWM_COUNTER_LSB_PTR     ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__A0_REG)
       #define myPWM_CAPTURE_LSB         (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__F1_REG)
       #define myPWM_CAPTURE_LSB_PTR     ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__F1_REG)

   #else
        #if(CY_PSOC3) /* 8-bit address space */
            #if(myPWM_PWMModeIsCenterAligned)
               #define myPWM_PERIOD_LSB      (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
               #define myPWM_PERIOD_LSB_PTR  ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
            #else
               #define myPWM_PERIOD_LSB      (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__F0_REG)
               #define myPWM_PERIOD_LSB_PTR  ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__F0_REG)
            #endif /* (myPWM_PWMModeIsCenterAligned) */

            #define myPWM_COMPARE1_LSB       (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__D0_REG)
            #define myPWM_COMPARE1_LSB_PTR   ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__D0_REG)
            #define myPWM_COMPARE2_LSB       (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
            #define myPWM_COMPARE2_LSB_PTR   ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__D1_REG)
            #define myPWM_COUNTERCAP_LSB     (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__A1_REG)
            #define myPWM_COUNTERCAP_LSB_PTR ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__A1_REG)
            #define myPWM_COUNTER_LSB        (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__A0_REG)
            #define myPWM_COUNTER_LSB_PTR    ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__A0_REG)
            #define myPWM_CAPTURE_LSB        (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__F1_REG)
            #define myPWM_CAPTURE_LSB_PTR    ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__F1_REG)
        #else
            #if(myPWM_PWMModeIsCenterAligned)
               #define myPWM_PERIOD_LSB      (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
               #define myPWM_PERIOD_LSB_PTR  ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
            #else
               #define myPWM_PERIOD_LSB      (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_F0_REG)
               #define myPWM_PERIOD_LSB_PTR  ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_F0_REG)
            #endif /* (myPWM_PWMModeIsCenterAligned) */

            #define myPWM_COMPARE1_LSB       (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_D0_REG)
            #define myPWM_COMPARE1_LSB_PTR   ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_D0_REG)
            #define myPWM_COMPARE2_LSB       (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
            #define myPWM_COMPARE2_LSB_PTR   ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
            #define myPWM_COUNTERCAP_LSB     (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_A1_REG)
            #define myPWM_COUNTERCAP_LSB_PTR ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_A1_REG)
            #define myPWM_COUNTER_LSB        (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_A0_REG)
            #define myPWM_COUNTER_LSB_PTR    ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_A0_REG)
            #define myPWM_CAPTURE_LSB        (*(reg16 *) myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_F1_REG)
            #define myPWM_CAPTURE_LSB_PTR    ((reg16 *)  myPWM_PWMUDB_sP16_pwmdp_u0__16BIT_F1_REG)
        #endif /* (CY_PSOC3) */

       #define myPWM_AUX_CONTROLDP1          (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u1__DP_AUX_CTL_REG)
       #define myPWM_AUX_CONTROLDP1_PTR      ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u1__DP_AUX_CTL_REG)

   #endif /* (myPWM_Resolution == 8) */

   #define myPWM_COUNTERCAP_LSB_PTR_8BIT ( (reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__A1_REG)
   #define myPWM_AUX_CONTROLDP0          (*(reg8 *)  myPWM_PWMUDB_sP16_pwmdp_u0__DP_AUX_CTL_REG)
   #define myPWM_AUX_CONTROLDP0_PTR      ((reg8 *)   myPWM_PWMUDB_sP16_pwmdp_u0__DP_AUX_CTL_REG)

#endif /* (myPWM_UsingFixedFunction) */

#if(myPWM_KillModeMinTime )
    #define myPWM_KILLMODEMINTIME        (*(reg8 *)  myPWM_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    #define myPWM_KILLMODEMINTIME_PTR    ((reg8 *)   myPWM_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    /* Fixed Function Block has no Kill Mode parameters because it is Asynchronous only */
#endif /* (myPWM_KillModeMinTime ) */

#if(myPWM_DeadBandMode == myPWM__B_PWM__DBM_256_CLOCKS)
    #define myPWM_DEADBAND_COUNT         (*(reg8 *)  myPWM_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define myPWM_DEADBAND_COUNT_PTR     ((reg8 *)   myPWM_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define myPWM_DEADBAND_LSB_PTR       ((reg8 *)   myPWM_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
    #define myPWM_DEADBAND_LSB           (*(reg8 *)  myPWM_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
#elif(myPWM_DeadBandMode == myPWM__B_PWM__DBM_2_4_CLOCKS)
    
    /* In Fixed Function Block these bits are in the control blocks control register */
    #if (myPWM_UsingFixedFunction)
        #define myPWM_DEADBAND_COUNT         (*(reg8 *)  myPWM_PWMHW__CFG0)
        #define myPWM_DEADBAND_COUNT_PTR     ((reg8 *)   myPWM_PWMHW__CFG0)
        #define myPWM_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << myPWM_DEADBAND_COUNT_SHIFT)

        /* As defined by the Register Map as DEADBAND_PERIOD[1:0] in CFG0 */
        #define myPWM_DEADBAND_COUNT_SHIFT   (0x06u)
    #else
        /* Lower two bits of the added control register define the count 1-3 */
        #define myPWM_DEADBAND_COUNT         (*(reg8 *)  myPWM_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define myPWM_DEADBAND_COUNT_PTR     ((reg8 *)   myPWM_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define myPWM_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << myPWM_DEADBAND_COUNT_SHIFT)

        /* As defined by the verilog implementation of the Control Register */
        #define myPWM_DEADBAND_COUNT_SHIFT   (0x00u)
    #endif /* (myPWM_UsingFixedFunction) */
#endif /* (myPWM_DeadBandMode == myPWM__B_PWM__DBM_256_CLOCKS) */



#if (myPWM_UsingFixedFunction)
    #define myPWM_STATUS                 (*(reg8 *) myPWM_PWMHW__SR0)
    #define myPWM_STATUS_PTR             ((reg8 *) myPWM_PWMHW__SR0)
    #define myPWM_STATUS_MASK            (*(reg8 *) myPWM_PWMHW__SR0)
    #define myPWM_STATUS_MASK_PTR        ((reg8 *) myPWM_PWMHW__SR0)
    #define myPWM_CONTROL                (*(reg8 *) myPWM_PWMHW__CFG0)
    #define myPWM_CONTROL_PTR            ((reg8 *) myPWM_PWMHW__CFG0)
    #define myPWM_CONTROL2               (*(reg8 *) myPWM_PWMHW__CFG1)
    #define myPWM_CONTROL3               (*(reg8 *) myPWM_PWMHW__CFG2)
    #define myPWM_GLOBAL_ENABLE          (*(reg8 *) myPWM_PWMHW__PM_ACT_CFG)
    #define myPWM_GLOBAL_ENABLE_PTR      ( (reg8 *) myPWM_PWMHW__PM_ACT_CFG)
    #define myPWM_GLOBAL_STBY_ENABLE     (*(reg8 *) myPWM_PWMHW__PM_STBY_CFG)
    #define myPWM_GLOBAL_STBY_ENABLE_PTR ( (reg8 *) myPWM_PWMHW__PM_STBY_CFG)


    /***********************************
    *          Constants
    ***********************************/

    /* Fixed Function Block Chosen */
    #define myPWM_BLOCK_EN_MASK          (myPWM_PWMHW__PM_ACT_MSK)
    #define myPWM_BLOCK_STBY_EN_MASK     (myPWM_PWMHW__PM_STBY_MSK)
    
    /* Control Register definitions */
    #define myPWM_CTRL_ENABLE_SHIFT      (0x00u)

    /* As defined by Register map as MODE_CFG bits in CFG2*/
    #define myPWM_CTRL_CMPMODE1_SHIFT    (0x04u)

    /* As defined by Register map */
    #define myPWM_CTRL_DEAD_TIME_SHIFT   (0x06u)  

    /* Fixed Function Block Only CFG register bit definitions */
    /*  Set to compare mode */
    #define myPWM_CFG0_MODE              (0x02u)   

    /* Enable the block to run */
    #define myPWM_CFG0_ENABLE            (0x01u)   
    
    /* As defined by Register map as DB bit in CFG0 */
    #define myPWM_CFG0_DB                (0x20u)   

    /* Control Register Bit Masks */
    #define myPWM_CTRL_ENABLE            (uint8)((uint8)0x01u << myPWM_CTRL_ENABLE_SHIFT)
    #define myPWM_CTRL_RESET             (uint8)((uint8)0x01u << myPWM_CTRL_RESET_SHIFT)
    #define myPWM_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << myPWM_CTRL_CMPMODE2_SHIFT)
    #define myPWM_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << myPWM_CTRL_CMPMODE1_SHIFT)

    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define myPWM_CTRL2_IRQ_SEL_SHIFT    (0x00u)
    #define myPWM_CTRL2_IRQ_SEL          (uint8)((uint8)0x01u << myPWM_CTRL2_IRQ_SEL_SHIFT)

    /* Status Register Bit Locations */
    /* As defined by Register map as TC in SR0 */
    #define myPWM_STATUS_TC_SHIFT        (0x07u)   
    
    /* As defined by the Register map as CAP_CMP in SR0 */
    #define myPWM_STATUS_CMP1_SHIFT      (0x06u)   

    /* Status Register Interrupt Enable Bit Locations */
    #define myPWM_STATUS_KILL_INT_EN_MASK_SHIFT          (0x00u)
    #define myPWM_STATUS_TC_INT_EN_MASK_SHIFT            (myPWM_STATUS_TC_SHIFT - 4u)
    #define myPWM_STATUS_CMP2_INT_EN_MASK_SHIFT          (0x00u)
    #define myPWM_STATUS_CMP1_INT_EN_MASK_SHIFT          (myPWM_STATUS_CMP1_SHIFT - 4u)

    /* Status Register Bit Masks */
    #define myPWM_STATUS_TC              (uint8)((uint8)0x01u << myPWM_STATUS_TC_SHIFT)
    #define myPWM_STATUS_CMP1            (uint8)((uint8)0x01u << myPWM_STATUS_CMP1_SHIFT)

    /* Status Register Interrupt Bit Masks */
    #define myPWM_STATUS_TC_INT_EN_MASK              (uint8)((uint8)myPWM_STATUS_TC >> 4u)
    #define myPWM_STATUS_CMP1_INT_EN_MASK            (uint8)((uint8)myPWM_STATUS_CMP1 >> 4u)

    /*RT1 Synch Constants */
    #define myPWM_RT1_SHIFT             (0x04u)

    /* Sync TC and CMP bit masks */
    #define myPWM_RT1_MASK              (uint8)((uint8)0x03u << myPWM_RT1_SHIFT)
    #define myPWM_SYNC                  (uint8)((uint8)0x03u << myPWM_RT1_SHIFT)
    #define myPWM_SYNCDSI_SHIFT         (0x00u)

    /* Sync all DSI inputs */
    #define myPWM_SYNCDSI_MASK          (uint8)((uint8)0x0Fu << myPWM_SYNCDSI_SHIFT)

    /* Sync all DSI inputs */
    #define myPWM_SYNCDSI_EN            (uint8)((uint8)0x0Fu << myPWM_SYNCDSI_SHIFT)


#else
    #define myPWM_STATUS                (*(reg8 *)   myPWM_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define myPWM_STATUS_PTR            ((reg8 *)    myPWM_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define myPWM_STATUS_MASK           (*(reg8 *)   myPWM_PWMUDB_genblk8_stsreg__MASK_REG)
    #define myPWM_STATUS_MASK_PTR       ((reg8 *)    myPWM_PWMUDB_genblk8_stsreg__MASK_REG)
    #define myPWM_STATUS_AUX_CTRL       (*(reg8 *)   myPWM_PWMUDB_genblk8_stsreg__STATUS_AUX_CTL_REG)
    #define myPWM_CONTROL               (*(reg8 *)   myPWM_PWMUDB_genblk1_ctrlreg__CONTROL_REG)
    #define myPWM_CONTROL_PTR           ((reg8 *)    myPWM_PWMUDB_genblk1_ctrlreg__CONTROL_REG)


    /***********************************
    *          Constants
    ***********************************/

    /* Control Register bit definitions */
    #define myPWM_CTRL_ENABLE_SHIFT      (0x07u)
    #define myPWM_CTRL_RESET_SHIFT       (0x06u)
    #define myPWM_CTRL_CMPMODE2_SHIFT    (0x03u)
    #define myPWM_CTRL_CMPMODE1_SHIFT    (0x00u)
    #define myPWM_CTRL_DEAD_TIME_SHIFT   (0x00u)   /* No Shift Needed for UDB block */
    
    /* Control Register Bit Masks */
    #define myPWM_CTRL_ENABLE            (uint8)((uint8)0x01u << myPWM_CTRL_ENABLE_SHIFT)
    #define myPWM_CTRL_RESET             (uint8)((uint8)0x01u << myPWM_CTRL_RESET_SHIFT)
    #define myPWM_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << myPWM_CTRL_CMPMODE2_SHIFT)
    #define myPWM_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << myPWM_CTRL_CMPMODE1_SHIFT)

    /* Status Register Bit Locations */
    #define myPWM_STATUS_KILL_SHIFT          (0x05u)
    #define myPWM_STATUS_FIFONEMPTY_SHIFT    (0x04u)
    #define myPWM_STATUS_FIFOFULL_SHIFT      (0x03u)
    #define myPWM_STATUS_TC_SHIFT            (0x02u)
    #define myPWM_STATUS_CMP2_SHIFT          (0x01u)
    #define myPWM_STATUS_CMP1_SHIFT          (0x00u)

    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define myPWM_STATUS_KILL_INT_EN_MASK_SHIFT          (myPWM_STATUS_KILL_SHIFT)
    #define myPWM_STATUS_FIFONEMPTY_INT_EN_MASK_SHIFT    (myPWM_STATUS_FIFONEMPTY_SHIFT)
    #define myPWM_STATUS_FIFOFULL_INT_EN_MASK_SHIFT      (myPWM_STATUS_FIFOFULL_SHIFT)
    #define myPWM_STATUS_TC_INT_EN_MASK_SHIFT            (myPWM_STATUS_TC_SHIFT)
    #define myPWM_STATUS_CMP2_INT_EN_MASK_SHIFT          (myPWM_STATUS_CMP2_SHIFT)
    #define myPWM_STATUS_CMP1_INT_EN_MASK_SHIFT          (myPWM_STATUS_CMP1_SHIFT)

    /* Status Register Bit Masks */
    #define myPWM_STATUS_KILL            (uint8)((uint8)0x00u << myPWM_STATUS_KILL_SHIFT )
    #define myPWM_STATUS_FIFOFULL        (uint8)((uint8)0x01u << myPWM_STATUS_FIFOFULL_SHIFT)
    #define myPWM_STATUS_FIFONEMPTY      (uint8)((uint8)0x01u << myPWM_STATUS_FIFONEMPTY_SHIFT)
    #define myPWM_STATUS_TC              (uint8)((uint8)0x01u << myPWM_STATUS_TC_SHIFT)
    #define myPWM_STATUS_CMP2            (uint8)((uint8)0x01u << myPWM_STATUS_CMP2_SHIFT)
    #define myPWM_STATUS_CMP1            (uint8)((uint8)0x01u << myPWM_STATUS_CMP1_SHIFT)

    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define myPWM_STATUS_KILL_INT_EN_MASK            (myPWM_STATUS_KILL)
    #define myPWM_STATUS_FIFOFULL_INT_EN_MASK        (myPWM_STATUS_FIFOFULL)
    #define myPWM_STATUS_FIFONEMPTY_INT_EN_MASK      (myPWM_STATUS_FIFONEMPTY)
    #define myPWM_STATUS_TC_INT_EN_MASK              (myPWM_STATUS_TC)
    #define myPWM_STATUS_CMP2_INT_EN_MASK            (myPWM_STATUS_CMP2)
    #define myPWM_STATUS_CMP1_INT_EN_MASK            (myPWM_STATUS_CMP1)

    /* Datapath Auxillary Control Register bit definitions */
    #define myPWM_AUX_CTRL_FIFO0_CLR         (0x01u)
    #define myPWM_AUX_CTRL_FIFO1_CLR         (0x02u)
    #define myPWM_AUX_CTRL_FIFO0_LVL         (0x04u)
    #define myPWM_AUX_CTRL_FIFO1_LVL         (0x08u)
    #define myPWM_STATUS_ACTL_INT_EN_MASK    (0x10u) /* As defined for the ACTL Register */
#endif /* myPWM_UsingFixedFunction */

#endif  /* CY_PWM_myPWM_H */


/* [] END OF FILE */
