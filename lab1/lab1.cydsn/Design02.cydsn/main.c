/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"


int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
   
   // myPWM_Init();
    myPWM_Enable();
    myPWM_Start();
    myADC_Start();
    //
    
    for(;;)
    {
        /* Place your application code here. */
        myPWM_WriteCompare( (uint16_t)(myADC_Read32()) / 65);
        
    }

}

/* [] END OF FILE */
